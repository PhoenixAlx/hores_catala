
(defun hores_catala_message ()
  "Write the hores in catala"
  (interactive)
  (setq hores_text (format-time-string "%H"))
  (setq minutes_text (format-time-string "%M"))
  (setq hores (string-to-number hores_text))
  (setq minutes (string-to-number minutes_text))
  (setq hores_updated (+ hores 1))
  (setq hores_updated (if (> hores_updated 23) 0 hores_updated ))
  (setq hores_table
      #s(hash-table
         size 24
         test equal
         data (
               0 "dotze"
               1 "una"
               2 "dues"
               3 "tres"
               4 "quatre"
               5 "cinc"
               6 "sis"
               7 "set"
               8 "vuit"
               9 "nou"
               10 "deu"
               11 "onze"
               12 "dotze"
               13 "una"
               14 "dues"
               15 "tres"
               16 "quatre"
               17 "cinc"
               18 "sis"
               19 "set"
               20 "vuit"
               21 "nou"
               22 "deu"
               23 "onze"
	       )
	 )
      )
  (setq parts_dia_table
      #s(hash-table
         size 24
         test equal
         data (
               0 "de la nit"
               1 "de la matinada"
               2 "de la matinada"
               3 "de la matinada"
               4 "de la matinada"
               5 "de la matinada"
               6 "del matí"
               7 "del matí"
               8 "del matí"
               9 "del matí"
               10 "del matí"
               11 "del matí"
               12 "del migdia"
               13 "del migdia"
               14 "del migdia"
               15 "de la tarda"
               16 "de la tarda"
               17 "de la tarda"
               18 "de la tarda"
               19 "del vespre"
               20 "del vespre"
               21 "del vespre"
               22 "del vespre"
               23 "de la nit"
	       )
	 )
      )
   (setq quarts_table
      #s(hash-table
         size 3
         test equal
         data (
	       0 ""
               1 "un quart"
               2 "dos quarts"
               3 "tres quarts"
	       )
	 )
      )



  (setq  hores_article (if (= hores_updated 1) " la " " " ))
  (setq  hores_article (if (= hores_updated 13) " la " " " ))
  (setq  tocat (if (= (/ minutes 15) 1) "tocat" "tocats" ))
  (setq  quartes_i (if (= (/ minutes 15) 0) "" "i" ))
  (if (= minutes 0)
    (message "%s %s en punt %s" hores_article (gethash hores hores_table ) (gethash hores parts_dia_table ) )
    ;;quarts o no quarts
    (if (= (% minutes 15) 0)
	;;quarts
	(message "%s de%s %s" (gethash (/ minutes 15)  quarts_table) (gethash hores_updated hores_table ) (gethash hores parts_dia_table ))
      ;; no quarts
      ;;mig quarts o no mig quarts
      (if (= (% minutes 15) 7)
	;;mig es 7 or not
	(if (= minutes 7)
	      (message "mig quart de%s%s %s" hores_article (gethash hores_updated hores_table )  (gethash hores_updated parts_dia_table ))
    	      (message " %s i mig de%s%s %s" (gethash  (/ minutes 15) quarts_table )  hores_article (gethash hores_updated hores_table ) (gethash hores_updated parts_dia_table ))
	)
	;;not mig
	(if (< (- (% minutes 15) 7) 0)
	    ;;not mig
	    (if (> (- (% minutes 15) 7) 3)
		;; be tocades
		(message " %s  ben %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) tocat hores_article (gethash hores_updated hores_table )  (gethash hores_updated parts_dia_table ))
	      ;; tocades
	      (message " %s %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) tocat hores_article (gethash hores_updated hores_table ) (gethash hores_updated parts_dia_table ))
		)
	  ;; mitg
	  (if (> (- (% minutes 15) 7) 3)
		;; be tocades
		(message " %s %s mig ben %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) quartes_i tocat hores_article (gethash hores_updated hores_table ) (gethash hores_updated parts_dia_table ))
	      ;; tocades
	      (message " %s %s mig %s de%s%s %s" (gethash  (/ minutes 15) quarts_table ) quartes_i tocat hores_article (gethash hores_updated hores_table )  (gethash hores_updated parts_dia_table ))

	      )
        )

       )
      )
    )
  )